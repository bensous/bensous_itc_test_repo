# git_test

This is my first time using git along with PyCharm editor

## Prerequisites

A working PyCharm install + Markdown plugin

A working Git install

### Installing

Open a terminal and use the following command:
```
git clone https://bensous@bitbucket.org/bensous/bensous_itc_test_repo.git
```

Alternatively you can use the VCS > Git > Clone... menu in PyCharm

## Author

* **Jeremy Bensoussan** 


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details (dummy)

## Acknowledgments

* Ofir Chakon for his clear directions on how to use Git
* the HIVE platform for allowing to steadily progress in the course
* of course, the fabulous team of Checkers
